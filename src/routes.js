const express = require('express');

const RoleController = require('./controllers/RoleController');
const UserController = require('./controllers/UserController');
const LoginController = require('./controllers/LoginController');
const CourseController = require('./controllers/CourseController');
const AddressController = require('./controllers/AddressController');
const StudentController = require('./controllers/StudentController');
const InstructorController = require('./controllers/InstructorController');
const ClasseController = require('./controllers/ClasseController');

const { eAdmin } = require('./middlewares/auth');

const routes = express.Router();

routes.get('/roles', RoleController.list);
routes.get('/role/:id', RoleController.view);
routes.post('/role', RoleController.store);
routes.put('/role', RoleController.edit);
routes.delete('/role/:id', RoleController.delete);
routes.post('/findOrCreateRole', RoleController.findOrCreate);

routes.get('/users', UserController.list);
routes.get('/user/:id', UserController.view);
routes.post('/user', UserController.store);
routes.put('/user', UserController.edit);
routes.delete('/user/:id', UserController.delete);

routes.get('/instructors', InstructorController.list);
routes.get('/instructor/:id', InstructorController.view);

routes.get('/courses', CourseController.list);
routes.get('/course/:id', CourseController.view);
routes.post('/course', CourseController.store);
routes.put('/course', eAdmin, CourseController.edit);
routes.delete('/course/:id', eAdmin, CourseController.delete);


routes.post('/classe', ClasseController.store);
routes.get('/classes', ClasseController.list);

routes.post('/address', AddressController.store);
routes.get('/addresses', AddressController.list);
routes.get('/address/:id', AddressController.view);
routes.put('/address', AddressController.edit);
routes.put('/address', AddressController.delete);

routes.post('/student', StudentController.store);
routes.get('/students', StudentController.list);
routes.get('/student/:id', StudentController.view);
routes.put('/student', StudentController.edit);
routes.delete('/student', StudentController.delete);

routes.post('/login', LoginController.auth);
routes.get('/val-token', eAdmin, LoginController.valToken);

module.exports = routes;

