const Address = require('../models/Address');

module.exports = {

    async list(req, res) {
        await Address.findAll({ order: [['id', 'DESC']] }).then(function (addresses) {
            return res.json({
                erro: false,
                addresses
            });
        }).catch(function () {
            return res.json({
                erro: true,
                mensagem: "Erro: Nenhum Curso encontrado!"
            });
        });
    },

    async view(req, res) {

        await Address.findByPk(req.params.id).
            then(address => {
                return res.json({
                    erro: false,
                    address
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    messagem: "Erro: Curso não encontrado!"
                });
            });
    },

    async store(req, res) {

        var data = req.body;

        await Address.create(data).then(function () {
            return res.json({
                erro: false,
                mensagem: "Endereço cadastrado com sucesso!"
            });
        }).catch(function () {
            return res.json({
                erro: true,
                mensagem: "Endereço não cadastrado com sucesso!"
            });
        });
    },

    async edit(req, res) {

        var data = req.body;

        await Address.update(data, { where: { id: data.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Curso editado com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Curso não editado com sucesso!"
                });
            });
    },

    async delete(req, res) {
        await Address.destroy({ where: { id: req.params.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Curso apagado com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Curso não apagado com sucesso!"
                });
            });
    }
}

