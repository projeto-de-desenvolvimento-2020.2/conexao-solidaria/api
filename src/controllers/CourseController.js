const Course = require('../models/Course');

module.exports = {

    async list(req, res) {
        await Course.findAll({ order: [['id', 'DESC']] }).then(function (courses) {
            return res.json({
                erro: false,
                mensagem: "Curso(s) encontrado(s)!",
                courses
            });
        });
    },

    async view(req, res) {
        await Course.findByPk(req.params.id).then(course => {
            return res.json({
                erro: false,
                mensagem: "Curso encontrado!",
                course
            });
        }).catch(function () {
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Curso não encontrado!"
            });
        });
    },

    async store(req, res) {
        var data = req.body;

        await Course.create(data).then(function () {
            return res.json({
                erro: false,
                mensagem: "Curso cadastrado com sucesso!"
            });
        }).catch(function () {
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Curso não cadastrado com sucesso!"
            });
        });
    },

    async edit(req, res) {

        var data = req.body;

        await Course.update(data, { where: { id: data.id } })
            .then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Curso editado com sucesso!"
                });
            }).catch(function () {
                return res.status(400).json({
                    erro: true,
                    mensagem: "Erro: Curso não editado com sucesso!"
                });
            });
    },

    async delete(req, res) {
        await Course.destroy({ where: {id: req.params.id}})
        .then(function(){
            return res.json({
                erro: false,
                mensagem: "Curso apagado com sucesso!"
            });
        }).catch(function(){
            return res.status(400).json({
                erro: true,
                mensagem: "Erro: Curso não apagado com sucesso!"
            });
        });
    }
}