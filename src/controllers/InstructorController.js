const User = require('../models/User');
const Role = require('../models/Role');
const Instructor = require('../models/Instructor');

const bcrypt = require('bcryptjs');

module.exports = {

    async list(req, res) {

        const instructors = await User.findAll({ attributes: ['id', 'role_id', 'name', 'email'], include: { association: 'role', attributes: ['rl'], where: { rl: 'Instrutor' } } })
        return res.json({
            erro: false,
            mensagem: "Instrutor(es) encontrado(s)!",
            instructors
        });
    },

    async view(req, res) {

        await User.findByPk(req.params.id, { attributes: ['id', 'role_id', 'name', 'email'], include: { association: 'role', attributes: ['rl'], where: { rl: 'Instrutor' } } }).
            then(instructor => {
                return res.json({
                    erro: false,
                    instructor
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    messagem: "Erro: Usuário não encontrado!"
                });
            });
    },

    async store(req, res) {

        var data = req.body;

        data.password = await bcrypt.hash(data.password, 8);

        role = await Role.findByPk(data.role_id);

        if (!role) {
            return res.json({
                erro: true,
                mensagem: "Erro: Role não encontrada!"
            })
        }

        await User.create(data).then(function () {
            return res.json({
                erro: false,
                mensagem: "Instrutor cadastrado com sucesso!"
            });
        }).catch(function () {
            return res.json({
                erro: true,
                mensagem: "Erro: Instrutor não cadastrado com sucesso!"
            });
        });
    },

    async valPass(req, res) {
        const { id, password } = req.body;

        var passCrypt = await bcrypt.hash(password, 8);

        await User.update({ password: passCrypt }, { where: { id } })
            .then(() => {
                return res.json({
                    erro: false,
                    mensagem: "Senha editada com sucesso!"
                });

            }).catch(() => {
                return res.status(400).json({
                    erro: true,
                    mensagem: "Erro: Senha não editada com sucesso!"
                });
            });
    },

    async valToken(req, res) {
        await User.findByPk(req.userId, { attributes: ['id', 'name', 'email'] })
            .then((user) => {
                return res.json({
                    erro: false,
                    user
                });
            }).catch(() => {
                return res.status(400).json({
                    erro: true,
                    mensagem: "Erro: Necessário realizar o login para acessar a página!"
                });
            });
    },

    async edit(req, res) {
        var data = req.body;
        data.password = await bcrypt.hash(data.password, 8);

        await User.update(data, { where: { id: data.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Instrutor editado com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Instrutor não editado com sucesso!"
                });
            });
    },

    async delete(req, res) {
        await User.destroy({ where: { id: req.params.id } }).
            then(function () {
                return res.json({
                    erro: false,
                    mensagem: "Instrutor apagado com sucesso!"
                });
            }).catch(function () {
                return res.json({
                    erro: true,
                    mensagem: "Erro: Instrutor não apagado com sucesso!"
                });
            });
    },

    async reload(req, res) {
        
    }

}

