const { Model, DataTypes, UUID } = require('sequelize');

class Student extends Model {
  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      cpf: DataTypes.STRING,
      rg: DataTypes.STRING,
      issuer_uf: DataTypes.STRING,
      start_date: DataTypes.DATE,
      final_date: DataTypes.DATE,
      grade: DataTypes.DOUBLE,
      frequency: DataTypes.DOUBLE,
      situation: DataTypes.STRING,
    }, {
      sequelize,
      tableName: "students",
      email: UUID,
      cpf: UUID,
      timestamps: true,
    })
  }
  static associate(models) {
   // this.belongsTo(models.Address, { foreignKey: 'address_id', as: 'address' });
   // this.hasMany(models.Registration, { foreignKey: 'student_id', as: 'registrations' });
   // this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
  }
}

module.exports = Student;