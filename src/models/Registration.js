const { Model, DataTypes, UUID } = require('sequelize');

class Registration extends Model {
  static init(sequelize) {
    super.init({
      grade: DataTypes.DOUBLE,
      frequency: DataTypes.DOUBLE,
      situation: DataTypes.STRING,
    }, {
      sequelize,
      tableName: "registrations",
      register: UUID,
      timestamps: true,
    })
  }
  static associate(models) {
   // this.belongsTo(models.Student, { foreignKey: 'student_id', as: 'student' });
   // this.belongsTo(models.CommunityClass, { foreignKey: 'community_class_id', as: 'community_class' });
  }
}

module.exports = Registration;
