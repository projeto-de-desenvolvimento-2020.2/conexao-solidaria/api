const { Model, DataTypes, UUID } = require('sequelize');

class Instructor extends Model {
  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      cpf: DataTypes.STRING,
      rg: DataTypes.STRING,
      issuer_uf: DataTypes.STRING,
      start_date: DataTypes.DATE,
      final_date: DataTypes.DATE
    }, {
      sequelize,
      tableName: "instructors",
      email: UUID,
      cpf: UUID,
      timestamps: true,
    })
  }
  static associate(models) {
    //this.hasMany(models.CommunityClass, { foreignKey: 'instructor_id', as: 'community_classes' });
   // this.belongsTo(models.Address, { foreignKey: 'address_id', as: 'address' });
   // this.belongsToMany(models.Course, { foreignKey: 'instructor_id', through: 'community_classes', as: 'courses' });
   // this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
  }
}

module.exports = Instructor;